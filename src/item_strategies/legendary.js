export function isLegendary(item) {
    return item.name === 'Sulfuras, Hand of Ragnaros';
}

export function updateLegendaryItem(item) {
    return isLegendary(item);
}
