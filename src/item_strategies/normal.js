import { decreaseQuality, decreaseSellIn, isSpoiled } from "../item_operators";

export function updateNormalItem(item) {
    decreaseSellIn(item);
    decreaseQuality(item);
    if (isSpoiled(item)) {
        decreaseQuality(item);
    }

    return true;
}
