import { decreaseSellIn, increaseQuality, isSpoiled } from "../item_operators";

export function isAcquiredTaste(item) {
    return item.name === 'Aged Brie';
}

export function updateAcquiredTasteItem(item) {
    if (!isAcquiredTaste(item)) {
        return false;
    }

    decreaseSellIn(item);
    increaseQuality(item);
    if (isSpoiled(item)) {
        increaseQuality(item);
    }

    return true;
}
