import { decreaseSellIn, increaseQuality, isSpoiled, setQualityToMin } from "../item_operators";

export function isTicket(item) {
    return item.name === 'Backstage passes to a TAFKAL80ETC concert';
}

export function isSellInLessThen10(item) {
    return item.sellIn < 10;
}

export function isSellInLessThen5(item) {
    return item.sellIn < 5;
}

export function updateTicketItem(item) {
    if (!isTicket(item)) {
        return false;
    }

    decreaseSellIn(item);
    increaseQuality(item);
    if (isSellInLessThen10(item)) {
        increaseQuality(item);
    }
    if (isSellInLessThen5(item)) {
        increaseQuality(item);
    }
    if (isSpoiled(item)) {
        setQualityToMin(item);
    }

    return true;
}
