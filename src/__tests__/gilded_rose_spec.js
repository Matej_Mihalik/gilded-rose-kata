import { Shop, Item } from '../gilded_rose';

const SELL_IN_SPOILED = -1;
const SELL_IN_TO_BE_SPOILED = 0;
const SELL_IN_GOOD = 1;
const SELL_IN_FROM_1_TO_5 = 1;
const SELL_IN_FROM_6_TO_10 = 6;
const SELL_IN_ABOVE_10 = 11;
const QUALITY_MIN = 0;
const QUALITY_1 = 1;
const QUALITY_2 = 2;
const QUALITY_3 = 3;
const QUALITY_42 = 42;
const QUALITY_BELOW_MAX = 49;
const QUALITY_MAX = 50;
const QUALITY_LEGENDARY = 80;

function getEmptyShop() {
    return new Shop();
}

function getNormalShop(sellIn, quality) {
    const itemName = 'Normal Item';
    return new Shop([new Item(itemName, sellIn, quality)]);
}

function getAcquiredTasteShop(sellIn, quality) {
    const itemName = 'Aged Brie';
    return new Shop([new Item(itemName, sellIn, quality)]);
}

function getTicketShop(sellIn, quality) {
    const itemName = 'Backstage passes to a TAFKAL80ETC concert';
    return new Shop([new Item(itemName, sellIn, quality)]);
}

function getLegendaryShop(sellIn) {
    const itemName = 'Sulfuras, Hand of Ragnaros';
    return new Shop([new Item(itemName, sellIn, QUALITY_LEGENDARY)]);
}

describe('Gilded Rose', function() {
    describe('Shop Defaults', function() {
        it('should init items to an empty array', function() {
            const gildedRose = getEmptyShop();

            expect(gildedRose.items).toHaveLength(0);
        });
    });

    describe('Sell In Decrease', function() {
        it('should decrease sell in for normal items', function() {
            const shop = getNormalShop(SELL_IN_GOOD, QUALITY_MIN);

            const item = shop.updateQuality()[0];

            expect(item.sellIn).toEqual(SELL_IN_TO_BE_SPOILED);
        });

        it('should decrease sell in for acquired taste items', function() {
            const shop = getAcquiredTasteShop(SELL_IN_GOOD, QUALITY_MIN);

            const item = shop.updateQuality()[0];

            expect(item.sellIn).toEqual(SELL_IN_TO_BE_SPOILED);
        });

        it('should decrease sell in for ticket items', function() {
            const shop = getTicketShop(SELL_IN_GOOD, QUALITY_MIN);

            const item = shop.updateQuality()[0];

            expect(item.sellIn).toEqual(SELL_IN_TO_BE_SPOILED);
        });

        it('should not decrease sell in for legendary items', function() {
            const shop = getLegendaryShop(SELL_IN_GOOD);

            const item = shop.updateQuality()[0];

            expect(item.sellIn).toEqual(SELL_IN_GOOD);
        });
    });

    describe("Quality Degradation", function() {
        it("should degrade normal item quality", function() {
            const shop = getNormalShop(SELL_IN_GOOD, QUALITY_1);

            const item = shop.updateQuality()[0];

            expect(item.quality).toEqual(QUALITY_MIN);
        });

        it("should degrade item quality twice as fast for spoiled items", function() {
            const shop = getNormalShop(SELL_IN_TO_BE_SPOILED, QUALITY_2);

            const item = shop.updateQuality()[0];

            expect(item.quality).toEqual(QUALITY_MIN);
        });
    });

    describe("Acquired Taste Items", function() {
        it("should upgrade acquired taste item quality", function() {
            const shop = getAcquiredTasteShop(SELL_IN_GOOD, QUALITY_MIN);

            const item = shop.updateQuality()[0];

            expect(item.quality).toEqual(QUALITY_1);
        });

        it("should upgrade quality twice as fast for spoiled acquired taste items", function() {
            const shop = getAcquiredTasteShop(SELL_IN_TO_BE_SPOILED, QUALITY_MIN);

            const item = shop.updateQuality()[0];

            expect(item.quality).toEqual(QUALITY_2);
        });
    });

    describe("Tickets", function() {
        it("should increase quality by one, when sell in is above 10", function() {
            const shop = getTicketShop(SELL_IN_ABOVE_10, QUALITY_MIN);

            const item = shop.updateQuality()[0];

            expect(item.quality).toEqual(QUALITY_1);
        });

        it("should increase quality by two, when sell in is between 6 and 10 inc.", function() {
            const shop = getTicketShop(SELL_IN_FROM_6_TO_10, QUALITY_MIN);

            const item = shop.updateQuality()[0];

            expect(item.quality).toEqual(QUALITY_2);
        });

        it("should increase quality by three, when sell in is between 1 and 5 inc.", function() {
            const shop = getTicketShop(SELL_IN_FROM_1_TO_5, QUALITY_MIN);

            const item = shop.updateQuality()[0];

            expect(item.quality).toEqual(QUALITY_3);
        });

        it("should drop the quality to zero after the event", function() {
            const shop = getTicketShop(SELL_IN_TO_BE_SPOILED, QUALITY_42);

            const item = shop.updateQuality()[0];

            expect(item.quality).toEqual(QUALITY_MIN);
        });
    });

    describe("Legendary Items", function() {
        it("should not degrade quality for legendary items", function() {
            const shop = getLegendaryShop(SELL_IN_GOOD);

            const item = shop.updateQuality()[0];

            expect(item.quality).toEqual(QUALITY_LEGENDARY);
        });

        it("should not degrade quality for spoiled legendary items", function() {
            const shop = getLegendaryShop(SELL_IN_SPOILED);

            const item = shop.updateQuality()[0];

            expect(item.quality).toEqual(QUALITY_LEGENDARY);
        });
    });

    describe("Min Quality", function() {
        it("should not lower item quality below zero", function() {
            const shop = getNormalShop(SELL_IN_GOOD, QUALITY_MIN);

            const item = shop.updateQuality()[0];

            expect(item.quality).toEqual(QUALITY_MIN);
        });

        it("should not lower spoiled item quality below zero", function() {
            const shop = getNormalShop(SELL_IN_TO_BE_SPOILED, QUALITY_MIN);

            const item = shop.updateQuality()[0];

            expect(item.quality).toEqual(QUALITY_MIN);
        });

        it("should not lower spoiled item quality below zero, when item has only one quality above the minimum", function() {
            const shop = getNormalShop(SELL_IN_TO_BE_SPOILED, QUALITY_1);

            const item = shop.updateQuality()[0];

            expect(item.quality).toEqual(QUALITY_MIN);
        });
    });

    describe("Max Quality ", function() {
        it("should not increase item quality above fifty", function() {
            const shop = getAcquiredTasteShop(SELL_IN_GOOD, QUALITY_MAX);

            const item = shop.updateQuality()[0];

            expect(item.quality).toEqual(QUALITY_MAX);
        });

        it("should not increase spoiled item quality above fifty", function() {
            const shop = getAcquiredTasteShop(SELL_IN_TO_BE_SPOILED, QUALITY_MAX);

            const item = shop.updateQuality()[0];

            expect(item.quality).toEqual(QUALITY_MAX);
        });

        it("should not increase spoiled item quality above fifty, when item is only one quality below the maximum", function() {
            const shop = getAcquiredTasteShop(SELL_IN_TO_BE_SPOILED, QUALITY_BELOW_MAX);

            const item = shop.updateQuality()[0];

            expect(item.quality).toEqual(QUALITY_MAX);
        });

        it("should not increase ticket quality above fifty, when sell in is above 10", function() {
            const shop = getTicketShop(SELL_IN_ABOVE_10, QUALITY_MAX);

            const item = shop.updateQuality()[0];

            expect(item.quality).toEqual(QUALITY_MAX);
        });

        it("should not increase ticket quality above fifty, when sell in is between 6 and 10 inc.", function() {
            const shop = getTicketShop(SELL_IN_FROM_6_TO_10, QUALITY_MAX);

            const item = shop.updateQuality()[0];

            expect(item.quality).toEqual(QUALITY_MAX);
        });

        it("should not increase ticket quality above fifty, when sell in is between 6 and 10 inc. and quality is only one below the maximum", function() {
            const shop = getTicketShop(SELL_IN_FROM_6_TO_10, QUALITY_BELOW_MAX);

            const item = shop.updateQuality()[0];

            expect(item.quality).toEqual(QUALITY_MAX);
        });

        it("should not increase ticket quality above fifty, when sell in is between 1 and 5 inc.", function() {
            const shop = getTicketShop(SELL_IN_FROM_1_TO_5, QUALITY_MAX);

            const item = shop.updateQuality()[0];

            expect(item.quality).toEqual(QUALITY_MAX);
        });

        it("should not increase ticket quality above fifty, when sell in is between 1 and 5 inc. and quality is only one below the maximum", function() {
            const shop = getTicketShop(SELL_IN_FROM_1_TO_5, QUALITY_BELOW_MAX);

            const item = shop.updateQuality()[0];

            expect(item.quality).toEqual(QUALITY_MAX);
        });
    });
});
