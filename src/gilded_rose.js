import { updateNormalItem } from "./item_strategies/normal";
import { updateLegendaryItem } from "./item_strategies/legendary";
import { updateAcquiredTasteItem } from "./item_strategies/acquired_taste";
import { updateTicketItem } from "./item_strategies/ticket";

export class Item {
    constructor(name, sellIn, quality) {
        this.name = name;
        this.sellIn = sellIn;
        this.quality = quality;
    }
}

export function updateItemQuality(item) {
    updateLegendaryItem(item)
    || updateAcquiredTasteItem(item)
    || updateTicketItem(item)
    || updateNormalItem(item);
}

export class Shop {
    constructor(items = []) {
        this.items = items;
    }

    updateQuality() {
        this.items.forEach(updateItemQuality);

        return this.items;
    }
}
